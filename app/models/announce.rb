class Announce < ActiveRecord::Base
  belongs_to :user
  validates :announce_content, length: {maximum:200,minimum:1}
end
