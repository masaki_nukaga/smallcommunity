class Post < ActiveRecord::Base
  belongs_to :user
  validates :post_content, length: {maximum:200,minimum:1}
end
