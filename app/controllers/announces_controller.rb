class AnnouncesController < ApplicationController
  before_action :authenticate_user!, only: [:show, :index]

  def index
    @announce = Announce.new
    @announces = Announce.all
  end

  def create
    announce = Announce.new(announce_params)
    announce.user = current_user
    announce.save
    redirect_to announces_path
  end

  private

  def announce_params
    params[:announce].permit(:announce_content)
  end
end
