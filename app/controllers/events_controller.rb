class EventsController < ApplicationController
  before_action :authenticate_user!, only: [:show, :index]

  def index
    @event_date = Date.today()
    @event = Event.new
    @events = Event.all
  end

  def create
    @event = Event.new(event_params)
    @event.user = current_user
    @event.save
    redirect_to events_path
  end

  def destroy
    @event = Event.find(params[:id])
    @event.destroy
    redirect_to events_path
  end

  private

  def event_params
    params[:event].permit(:event_title,:event_date,:event_content)
  end
end
