class PostsController < ApplicationController
  before_action :authenticate_user!, only: [:show, :index]

  def index
    @post = Post.new
    @posts = Post.all
  end

  def create
    post = Post.new(post_params)
    post.user = current_user
    post.save
    redirect_to posts_path
  end

  private

  def post_params
    params[:post].permit(:post_content)
  end
end
