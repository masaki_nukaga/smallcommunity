class CreateEvents < ActiveRecord::Migration
  def change
    create_table :events do |t|
      t.string :event_title
      t.datetime  :event_date
      t.text :event_content
      t.timestamps
    end
  end
end
