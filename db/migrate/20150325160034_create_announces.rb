class CreateAnnounces < ActiveRecord::Migration
  def change
    create_table :announces do |t|
      t.text :announce_content
      t.timestamps
    end
  end
end
